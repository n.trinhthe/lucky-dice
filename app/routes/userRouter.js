const express = require('express');
const userRouter = express.Router();
const { createUser, getAllUser, getUserById, updateUserById, deleteUserById } = require('../controllers/userController');

userRouter.post('/users',createUser);
userRouter.get('/users',getAllUser);
userRouter.get('/users/:userId',getUserById);
userRouter.put('/users/:userId',updateUserById);
userRouter.delete('/users/:userId',deleteUserById);

module.exports = userRouter;