const express = require('express');
const voucherHistoryRouter = express.Router();
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById, getVouchersOfUser } = require('../controllers/voucherHistoryController');

voucherHistoryRouter.post('/voucher-histories',createVoucherHistory);
voucherHistoryRouter.get('/voucher-histories',getAllVoucherHistory);
voucherHistoryRouter.get('/voucher-histories/:historyId',getVoucherHistoryById);
voucherHistoryRouter.put('/voucher-histories/:historyId',updateVoucherHistoryById);
voucherHistoryRouter.delete('/voucher-histories/:historyId',deleteVoucherHistoryById);
voucherHistoryRouter.get('/devcamp-lucky-dice/voucher-history',getVouchersOfUser);

module.exports = voucherHistoryRouter;