const express = require('express');
const prizeHistoryRouter = express.Router();
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById, getPrizesOfUser } = require('../controllers/prizeHistoryController');

prizeHistoryRouter.post('/prize-histories',createPrizeHistory);
prizeHistoryRouter.get('/prize-histories',getAllPrizeHistory);
prizeHistoryRouter.get('/prize-histories/:historyId',getPrizeHistoryById);
prizeHistoryRouter.put('/prize-histories/:historyId',updatePrizeHistoryById);
prizeHistoryRouter.delete('/prize-histories/:historyId',deletePrizeHistoryById);
prizeHistoryRouter.get('/devcamp-lucky-dice/prize-history',getPrizesOfUser);

module.exports = prizeHistoryRouter;
