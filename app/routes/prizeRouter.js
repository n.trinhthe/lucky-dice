const express = require('express');
const prizeRouter = express.Router();
const { createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById } = require('../controllers/prizeController');

prizeRouter.post('/prizes',createPrize);
prizeRouter.get('/prizes',getAllPrize);
prizeRouter.get('/prizes/:prizeId',getPrizeById);
prizeRouter.put('/prizes/:prizeId',updatePrizeById);
prizeRouter.delete('/prizes/:prizeId',deletePrizeById);

module.exports = prizeRouter;