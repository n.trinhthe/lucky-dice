const  express = require('express');
const diceRouter = express.Router();
const { diceHandler } = require ('../controllers/diceController');

diceRouter.post('/devcamp-lucky-dice/dice',diceHandler);

module.exports = diceRouter;