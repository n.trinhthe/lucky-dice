const express = require('express');
const diceHistoryRouter = express.Router();
const { createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById, getDicesOfUser } = require('../controllers/diceHistoryController');

diceHistoryRouter.post('/dice-histories',createDiceHistory);
diceHistoryRouter.get('/dice-histories',getAllDiceHistory);
diceHistoryRouter.get('/dice-histories/:diceHistoryId',getDiceHistoryById);
diceHistoryRouter.put('/dice-histories/:diceHistoryId',updateDiceHistoryById);
diceHistoryRouter.delete('/dice-histories/:diceHistoryId',deleteDiceHistoryById);
diceHistoryRouter.get('/devcamp-lucky-dice/dice-history',getDicesOfUser);

module.exports = diceHistoryRouter;