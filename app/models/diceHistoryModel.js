const mongoose = require('mongoose');

const diceHistorySchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    user: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            require: true
    },
	dice: {
        type: Number,
        require: true
    }
},{
    timeStamp: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
})
module.exports = mongoose.model('DiceHistory',diceHistorySchema);
 