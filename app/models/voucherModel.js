const mongoose = require('mongoose');

const voucherSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    code: {
        type: String,
        unique: true,
        require: true
    },
	discount: {
        type: Number,
        require: true
    },
	note: {
        type: String,
        require: false
    }
},{
    timeStamp: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
})

module.exports = mongoose.model("Voucher",voucherSchema);