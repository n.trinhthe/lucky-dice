const mongoose = require('mongoose');

const prizeSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    name: {
        type: String,
        unique: true,
        require: true
    },
	description: {
        type: String,
        require: false
    },
},{
    timeStamp: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
})

module.exports = mongoose.model("Prize",prizeSchema);