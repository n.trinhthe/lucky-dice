const mongoose = require('mongoose');
 const voucherHistorySchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    user: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            require: true
        }
    ,
    voucher: {
            type: mongoose.Types.ObjectId,
            ref: "Voucher",
            require: true
    }
 },{
    timeStamp: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
})

module.exports = mongoose.model('VoucherHistory',voucherHistorySchema);