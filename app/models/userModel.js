const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    username: {
        type: String,
        unique: true,
        require: true
    },
	firstname: {
        type: String,
        require: true
    },
	lastname: {
        type: String,
        require: true
    }
},{
    timeStamp: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
})

module.exports = mongoose.model("User",userSchema);