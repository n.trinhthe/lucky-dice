const mongoose = require('mongoose');
 const prizeHistorySchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    user: {
            type: mongoose.Types.ObjectId,
            ref: "User",
            require: true
    },
    prize:{
            type: mongoose.Types.ObjectId,
            ref: "Prize",
            require: true
        }
 },{
    timeStamp: {
        createdAt: "createdAt",
        updatedAt: "updatedAt"
    }
})

module.exports = mongoose.model('PrizeHistory',prizeHistorySchema);