const mongoose = require('mongoose');
const userModel = require('../models/userModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');

//CREATE VOUCHER HISTORY
const createVoucherHistory  =(req,res)=>{
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Voucher History is empty"
        })
    }
    let  newVoucherHistory = new voucherHistoryModel({
        _id: mongoose.Types.ObjectId(),
    })
    voucherHistoryModel.create(newVoucherHistory,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(201).json({
                message: `CREATE VOUCHER HISTORY SUCCESS`,
                prize: data
            })
        }
    })
}
//GET ALL VOUCHER HISTORY
const getAllVoucherHistory = (req,res)=>{
    let user = req.query.user; 
    const condition = {};
    if (user){
        condition.user = user;
    }
    voucherHistoryModel.find(condition,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET ALL VOUCHER HISTORY SUCCESS`,
                vouchers: data
            })
        }
    })
}
//GET VOUCHER HISTORY BY ID
const getVoucherHistoryById = (req,res)=>{
    let historyId = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(500).json({
            message: `Voucher History ID is not valid`
        })
    }
    voucherHistoryModel.findById(historyId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET VOUCHER HISTORY BY ID SUCCESS`,
                prize: data
            })
        }
    })
}
//UPDATE VOUCHER HISTORY
const updateVoucherHistoryById = (req,res)=>{
    let historyId = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(500).json({
            message: `Voucher History ID is not valid`
        })
    }
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Voucher History is empty"
        })
    }
    let  newVoucherHistory = new voucherHistoryModel({
    })
    voucherHistoryModel.findByIdAndUpdate(historyId,newVoucherHistory,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `UDPATE VOUCHER HISTORY BY ID SUCCESS`,
                prize: data
            })
        }
    })
}
//DELETE VOUCHER HISTORY
const deleteVoucherHistoryById = (req,res)=>{
    let historyId = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(500).json({
            message: `Voucher History ID is not valid`
        })
    }
    voucherHistoryModel.findByIdAndDelete(historyId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(204).json({
                message: `DELETE VOUCHER HISTORY BY ID SUCCESS`,
                prize: data
            })
        }
    })
}
//GET VOUCHER BY USERNAME
const getVouchersOfUser = (req,res)=>{
    let username = req.query.username;
    userModel.find({username}).exec((errFindUser,dataFindUser)=>{
        if(errFindUser){
            return res.status(500).json({
                message: `Error Internal: ${errFindUser.message}`
            })
        }
        else {
            if(!dataFindUser){
                return res.status(404).json({
                    status: 404,
                    message: `Not Find User`,
                    vouchers: []
                })
            }
            else {
                voucherHistoryModel.find({user: dataFindUser})
                .populate('voucher')
                .populate('user')
                .exec((errVoucherHistory,dataVoucherHistory)=>{
                    if(errVoucherHistory){
                        return res.status(500).json({
                            message: `Error Internal: ${errVoucherHistory.message}`
                        })
                    }
                    else {
                        return res.status(200).json({
                            status: 200,
                            message: `Get Voucher History Of User Success`,
                            vouchers: dataVoucherHistory
                        })
                    }
                })
            }
        }
    })
}
module.exports = {
    createVoucherHistory,
    getAllVoucherHistory,
    getVoucherHistoryById,
    updateVoucherHistoryById,
    deleteVoucherHistoryById,
    getVouchersOfUser
}