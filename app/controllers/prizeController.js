const mongoose = require('mongoose');
const prizeModel  = require ('../models/prizeModel');

const createPrize = (req,res) => {
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Prize is empty"
        })
    }
    let  newPrize = new prizeModel({
        _id: mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    })
    prizeModel.create(newPrize,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(201).json({
                message: `CREATE PRIZE SUCCESS`,
                prize: data
            })
        }
    })
}
//GET ALL PRIZE
const getAllPrize = (req,res)=>{
    prizeModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET ALL PRIZE SUCCESS`,
                prizes: data
            })
        }
    })
}
//GET PRIZE BY ID
const getPrizeById = (req,res)=>{
    let prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(500).json({
            message: `Prize ID is not valid`
        })
    }
    prizeModel.findById(prizeId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET PRIZE BY ID SUCCESS`,
                prizes: data
            })
        }
    })
}
//UPDATE PRIZE
const updatePrizeById   = (req,res)=>{
    let prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(500).json({
            message: `Prize ID is not valid`
        })
    }
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Prize is empty"
        })
    }
    let  newPrize = new prizeModel({
        name: body.name,
        description: body.description
    })
    prizeModel.findByIdAndUpdate(prizeId,newPrize,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `UPDATE PRIZE BY ID SUCCESS`,
                prizes: data
            })
        }
    })
}
//DELETE PRIZE
const deletePrizeById = (req,res)=>{
    let prizeId = req.params.prizeId;
    if (!mongoose.Types.ObjectId.isValid(prizeId)){
        return res.status(500).json({
            message: `Prize ID is not valid`
        })
    }
    prizeModel.findByIdAndDelete(prizeId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(204).json({
                message: `DELETE PRIZE BY ID SUCCESS`,
            })
        }
    })
}
module.exports = {
    createPrize,
    getAllPrize,
    getPrizeById,
    updatePrizeById,
    deletePrizeById
}
