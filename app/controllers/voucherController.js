const mongoose = require('mongoose');
const voucherModel = require('../models/voucherModel');

//CREATE VOUCHER
const createVoucher = (req,res)=>{
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Voucher is empty"
        })
    }
    let  newVoucher = new voucherModel({
        _id: mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note,
    })
    voucherModel.create(newVoucher,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(201).json({
                message: `CREATE VOUCHER SUCCESS`,
                voucher: data
            })
        }
    })
}
//GET ALL VOUCHER
const getAllVoucher = (req,res)=>{
    voucherModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET ALL VOUCHER SUCCESS`,
                voucher: data
            })
        }
    })
}
//GET VOUCHER BY ID
const getVoucherById = (req,res)=>{
    let voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(500).json({
            message: `Voucher ID is not valid`
        })
    }
    voucherModel.findById(voucherId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET VOUCHER BY ID SUCCESS`,
                voucher: data
            })
        }
    })
}
//UPDATE VOUCHER BY ID
const updateVoucherById = (req,res)=>{
    let voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(500).json({
            message: `Voucher ID is not valid`
        })
    }
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Voucher is empty"
        })
    }
    let  newVoucher = new voucherModel({
        code: body.code,
        discount: body.discount,
        note: body.note,
    })
    voucherModel.findByIdAndUpdate(voucherId,newVoucher,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `UPDATE VOUCHER BY ID SUCCESS`,
                voucher: data
            })
        }
    })
}
//DELETE VOUCHER 
const deleteVoucherById = (req,res)=>{
    let voucherId = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(500).json({
            message: `Voucher ID is not valid`
        })
    }
    voucherModel.findByIdAndDelete(voucherId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(204).json({
                message: `DELETE VOUCHER BY ID SUCCESS`,
                voucher: data
            })
        }
    })
}

module.exports = {
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}