const mongoose = require('mongoose');
const diceHistoryModel = require('../models/diceHistoryModel');
const userModel = require('../models/userModel');

//CREATE DICE HISTORY
const createDiceHistory = (req, res) => {
    let body = req.body;
    if (!body) {
        return res.status(400).json({
            message: "Content Dice History is empty"
        })
    }
    let newDiceHistory = new diceHistoryModel({
        _id: mongoose.Types.ObjectId(),
        user: body.user,
        dice: body.dice,
    })
    diceHistoryModel.create(newDiceHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(201).json({
                message: `CREATE DICE HISTORY SUCCESS`,
                diceHistory: data
            })
        }
    })
}
//GET ALL DICE HISTORY
const getAllDiceHistory = (req, res) => {
    let user = req.query.user;
    const condition = {};
    if (user) {
        condition.user = user;
    }
    diceHistoryModel.find(condition, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET ALL DICE HISTORY SUCCESS`,
                diceHistories: data
            })
        }
    })
}
//GET DICE HISTORY BY ID
const getDiceHistoryById = (req, res) => {
    let diceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(500).json({
            message: `Dice History ID is not valid`
        })
    }
    diceHistoryModel.findById(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET DICE HISTORY BY ID SUCCESS`,
                user: data
            })
        }
    })
}
//UPDATE DICE HISTORY
const updateDiceHistoryById = (req, res) => {
    let diceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(500).json({
            message: `Dice History ID is not valid`
        })
    }
    let body = req.body;
    if (!body) {
        return res.status(400).json({
            message: "Content User is empty"
        })
    }
    let newDiceHistory = new diceHistoryModel({
        dice: body.dice,
    })
    diceHistoryModel.findByIdAndUpdate(diceHistoryId, newDiceHistory, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `UPDATE DICE HISTORY BY ID SUCCESS`,
                user: data
            })
        }
    })
}
//DELETE DICE HISTORY
const deleteDiceHistoryById = (req, res) => {
    let diceHistoryId = req.params.diceHistoryId;
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return res.status(500).json({
            message: `Dice History ID is not valid`
        })
    }
    diceHistoryModel.findByIdAndDelete(diceHistoryId, (err, data) => {
        if (err) {
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(204).json({
                message: `DELETE DICE HISTORY BY ID SUCCESS`,
            })
        }
    })
}
//Get Dice History By Username query
const getDicesOfUser = (req, res) => {
    let username = req.query.username;
    userModel.find({ username }).exec((errFindUser, dataFindUser) => {
        if (errFindUser) {
            return res.status(500).json({
                message: `Error Internal: ${errFindUser.message}`
            })
        }
        else {
            if (!dataFindUser) {
                return res.status(404).json({
                    status: 404,
                    message: "Not find User",
                    dices: []
                })
            }
            else {
                diceHistoryModel.find({ user: dataFindUser })
                    // .populate('user')
                    .exec((errDiceHistory, dataDiceHistory) => {
                        if (errDiceHistory) {
                            return res.status(500).json({
                                message: `Error Internal: ${errDiceHistory.message}`
                            })
                        }
                        else {
                            return res.status(200).json({
                                status: 200,
                                user: dataFindUser,
                                dices: dataDiceHistory,
                            })
                        }
                    })
            }
        }
    })
}
module.exports = {
    createDiceHistory,
    getAllDiceHistory,
    getDiceHistoryById,
    updateDiceHistoryById,
    deleteDiceHistoryById,
    getDicesOfUser
}