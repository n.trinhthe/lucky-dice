const mongoose = require('mongoose');
const userModel = require ('../models/userModel');

//CREATE USER
const createUser  = (req,res)=>{
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content User is empty"
        })
    }
    let  newUser = new userModel({
        _id: mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    })
    userModel.create(newUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(201).json({
                message: `CREATE USER SUCCESS`,
                user: data
            })
        }
    })
}
//GET ALL USER
const getAllUser  = (req,res)=>{
    userModel.find((err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET ALL USER SUCCESS`,
                users: data
            })
        }
    })
}
//GET USER BY ID
const getUserById = (req,res) =>{
    let userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            message: `User ID is not valid`
        })
    }
    userModel.findById(userId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET USER BY ID SUCCESS`,
                user: data
            })
        }
    })
}
//UPDATE USER
const updateUserById  = (req,res)=>{
    let userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            message: `User ID is not valid`
        })
    }
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content User is empty"
        })
    }
    let  newUser = new userModel({
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname,
    })
    userModel.findByIdAndUpdate(userId,newUser,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `UPDATE USER BY ID SUCCESS`,
                user: data
            })
        }
    })
}
//DELETE USER
const deleteUserById = (req,res)=>{
    let userId = req.params.userId;
    if (!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(500).json({
            message: `User ID is not valid`
        })
    }
    userModel.findByIdAndDelete(userId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(204).json({
                message: `DELETE USER BY ID SUCCESS`,
            })
        }
    })
}
module.exports = {
    createUser,
    getAllUser,
    getUserById,
    updateUserById,
    deleteUserById
}