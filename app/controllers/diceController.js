const mongoose = require('mongoose');
const userModel = require('../models/userModel');
const diceHistoryModel = require('../models/diceHistoryModel');
const voucherModel = require('../models/voucherModel');
const voucherHistoryModel = require('../models/voucherHistoryModel');
const prizeModel = require('../models/prizeModel');
const prizeHistoryModel = require('../models/prizeHistoryModel');

const diceHandler = (req, res) => {
    console.log(req.body)
    let username = req.body.username
    let firstname = req.body.firstname
    let lastname = req.body.lastname
    let randomDice = Math.floor(Math.random() * 6 + 1);
    if (!username) {
        return res.status(400).json({
            message: "Username is empty",
            status: 400,
        })
    }
    if (!firstname) {
        return res.status(400).json({
            message: "Firstname is empty",
            status: 400,
        })
    }
    if (!lastname) {
        return res.status(400).json({
            message: "Lastname is empty",
            status: 400,
        })
    }
    userModel.findOne({ username }, (errFindUser, dataFindUser) => {
        if (errFindUser) {
            return res.status(500).json({
                message: `Error Internal: ${errFindUser.message}`
            })
        }
        else {
            if (!dataFindUser) {
                userModel.create({
                    _id: mongoose.Types.ObjectId(),
                    username,
                    firstname,
                    lastname
                }, (errUserCreated, dataUserCreated) => {
                    if (errUserCreated) {
                        return res.status(500).json({
                            message: `Error Internal: ${errUserCreated.message}`
                        })
                    }
                    else {
                        diceHistoryModel.create({
                            _id: mongoose.Types.ObjectId(),
                            user: dataUserCreated._id,
                            dice: randomDice,
                        }, (errDiceHistoryCreate, dataDiceHistoryCreate) => {
                            if (errDiceHistoryCreate) {
                                return res.status(500).json({
                                    message: `Error Internal: ${errDiceHistoryCreate.message}`
                                })
                            }
                            else {
                                //Nếu dice < 3 không ra giải thưởng, voucher 
                                if (randomDice <= 3) {
                                    return res.status(200).json({
                                        dice: randomDice,
                                        voucher: null,
                                        prize: null
                                    })
                                }
                                //Nếu dice > 3 thì random giải thưởng, voucher
                                else {
                                    voucherModel.count().exec((errCountVoucher, countVoucher) => {
                                        let randomVoucher = Math.floor(Math.random() * countVoucher)
                                        voucherModel.findOne().skip(randomVoucher).exec((errFindVoucher, dataFindVoucher) => {
                                            if (errFindVoucher) {
                                                return res.status(500).json({
                                                    message: `Error Internal: ${errFindVoucher}`
                                                })
                                            } else {
                                                prizeHistoryModel.create({
                                                    _id: mongoose.Types.ObjectId(),
                                                    user: dataUserCreated._id,
                                                    voucher: dataFindVoucher._id
                                                }, (errVoucherHistoryCreated, dataVoucherHistoryCreated) => {
                                                    if (errVoucherHistoryCreated) {
                                                        return res.status(500).json({
                                                            message: `Error Internal: ${errVoucherHistoryCreated}`
                                                        })
                                                    }
                                                    else {
                                                        return res.status(200).json({
                                                            dice: randomDice,
                                                            prize: null,
                                                            voucher: dataFindVoucher
                                                        })
                                                    }
                                                })
                                            }
                                        })

                                    })
                                }
                            }
                        })
                    }
                })
            }
            else {
                diceHistoryModel.create({
                    _id: mongoose.Types.ObjectId(),
                    user: dataFindUser._id,
                    dice: randomDice,
                }, (errDiceHistoryCreate, dataDiceHistoryCreate) => {
                    if (errDiceHistoryCreate) {
                        return res.status(500).json({
                            message: `Error Internal: ${errFindUser}`
                        })
                    }
                    else {
                        //
                        if (randomDice <= 3) {
                            return res.status(200).json({
                                dice: randomDice,
                                voucher: null,
                                prize: null
                            })
                        }
                        else {
                            voucherModel.count().exec((errCountVoucher, countVoucher) => {
                                let randomVoucher = Math.floor(Math.random() * countVoucher)
                                voucherModel.findOne().skip(randomVoucher).exec((errFindVoucher, dataFindVoucher) => {
                                    if (errFindVoucher) {
                                        return res.status(500).json({
                                            message: `Error Internal: ${errFindVoucher.message}`
                                        })
                                    }
                                    else {
                                        voucherHistoryModel.create({
                                            _id: mongoose.Types.ObjectId(),
                                            user: dataFindUser._id,
                                            voucher: dataFindVoucher._id
                                        }, (errVoucherHistoryCreated, dataVoucherHistoryCreated) => {
                                            if (errVoucherHistoryCreated) {
                                                return res.status(500).json({
                                                    message: `Error Internal: ${errVoucherHistoryCreated}`
                                                })
                                            }
                                            else {
                                                //Lấy 3 lần giao súc sắc gân nhất
                                                diceHistoryModel.find().sort({ _id: -1 }).limit(3).exec((errLast3DiceHistory, dataLast3DiceHistory) => {
                                                    if (errLast3DiceHistory) {
                                                        return res.status(500).json({
                                                            message: `Error Internal: ${errLast3DiceHistory.message}`
                                                        })
                                                    }
                                                    else {
                                                        if (dataLast3DiceHistory.length < 3) {
                                                            return res.status(200).json({
                                                                dice: randomDice,
                                                                prize: null,
                                                                voucher: dataFindVoucher
                                                            })
                                                        }
                                                        else {
                                                            prizeModel.count().exec((errCountPrize, countPrize) => {
                                                                let randomPrize = Math.floor(Math.random() * countPrize);
                                                                prizeModel.findOne().skip(randomPrize).exec((errFindPrize, dataFindPrize) => {
                                                                    if (errFindPrize){
                                                                        return res.status(500).json({
                                                                            message: `Error Internal: ${errFindPrize.message}`
                                                                        })
                                                                    }
                                                                    else {
                                                                        prizeHistoryModel.create({
                                                                            _id: mongoose.Types.ObjectId(),
                                                                            user: dataFindUser._id,
                                                                            prize: dataFindPrize._id
                                                                        }, (errPrizeHistoryCreated, dataPrizeHistoryCreated) => {
                                                                            if (errPrizeHistoryCreated){
                                                                                return res.status(500).json({
                                                                                    message: `Error Internal: ${errPrizeHistoryCreated.message}`
                                                                                })
                                                                            }
                                                                            else {
                                                                                return res.status(200).json({
                                                                                    dice: randomDice,
                                                                                    prize: dataFindPrize,
                                                                                    voucher: dataFindVoucher
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                    
                                                                })
                                                            })
                                                        }
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })

                            })
                        }
                    }
                })
            }
        }
    })
}
module.exports = { diceHandler }