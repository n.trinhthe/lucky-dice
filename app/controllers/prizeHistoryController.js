const mongoose = require('mongoose');
const { populate } = require('../models/prizeHistoryModel');
const prizeHistoryModel = require('../models/prizeHistoryModel');
const userModel = require('../models/userModel');
//CREATE PRIZE HISTORY
const createPrizeHistory = (req,res)=>{
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Prize History is empty"
        })
    }
    let  newPrizeHistory = new prizeHistoryModel({
        _id: mongoose.Types.ObjectId(),
    })
    prizeHistoryModel.create(newPrizeHistory,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(201).json({
                message: `CREATE PRIZE HISTORY SUCCESS`,
                prize: data
            })
        }
    })
}
//GET ALL PRIZE HISTORY
const getAllPrizeHistory = (req,res)=>{
    let condition = {};
    let user = req.query.user;
    if (user){
        condition.user = user;
    }
    prizeHistoryModel.find(condition,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET ALL PRIZE HISTORY SUCCESS`,
                prize: data
            })
        }
    })
}
//GET PRIZE HISTORY BY ID
const getPrizeHistoryById  = (req,res)=>{
    let historyId = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(500).json({
            message: `Prize History ID is not valid`
        })
    }
    prizeHistoryModel.findById(historyId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `GET PRIZE HISTORY BY ID SUCCESS`,
                prize: data
            })
        }
    })
}
//UPDATE PRIZE HISTORY
const  updatePrizeHistoryById = (req,res)=>{
    let historyId = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(500).json({
            message: `Prize History ID is not valid`
        })
    }
    let body = req.body;
    if(!body){
        return res.status(400).json({
            message: "Content Prize History is empty"
        })
    }
    let  newPrizeHistory = new prizeHistoryModel({

    })
    prizeHistoryModel.findByIdAndUpdate(historyId,newPrizeHistory,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(200).json({
                message: `UPDATE PRIZE HISTORY BY ID SUCCESS`,
                prize: data
            })
        }
    })
}
//DELTE PRIZE HISTORY
const deletePrizeHistoryById = (req,res)=>{
    let historyId = req.params.historyId;
    if (!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(500).json({
            message: `Prize History ID is not valid`
        })
    }
    prizeHistoryModel.findByIdAndDelete(historyId,(err,data)=>{
        if(err){
            return res.status(500).json({
                message: `Error Internal: ${err.message}`
            })
        }
        else {
            return res.status(204).json({
                message: `DELTE PRIZE HISTORY BY ID SUCCESS`,
            })
        }
    })
}
//Get Prize History by Query Username
const getPrizesOfUser = (req,res) =>{
    let username = req.query.username;
    userModel.find({username: username},(errFindUser,dataFindUser)=>{
        if(errFindUser){
            return res.status(500).json({
                message: `Error Internal: ${errFindUser.message}`
            })
        }
        else {
            if(!dataFindUser){
                return res.status(404).json({
                    status:404,
                    user: `Not Find User`,
                    dices: []
                })
            }
            else {
                prizeHistoryModel.find({user: dataFindUser})
                .populate('user')
                .populate('prize')
                .exec((errPrizeHistory,dataPrizeHistory)=>{
                    if(errPrizeHistory){
                        return res.status(500).json({
                            message: `Error Internal: ${errPrizeHistory.message}`
                        })
                    }
                    else {
                        return res.status(200).json({
                            status:200,
                            user: dataFindUser,
                            prizes: dataPrizeHistory
                        })
                    }
                })
            }
        }
    })
}
module.exports = {
    createPrizeHistory,
    getAllPrizeHistory,
    getPrizeHistoryById,
    updatePrizeHistoryById,
    deletePrizeHistoryById,
    getPrizesOfUser
}