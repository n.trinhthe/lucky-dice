const express = require("express");
const app = express();
const path = require("path");
const mongoose = require('mongoose')
const { luckyDiceMiddleware } = require("./app/middlewares/luckyDiceMiddleware")
const { userSchema } = require('./app/models/userModel');
const { diceHistorySchema } = require('./app/models/diceHistoryModel');
const { prizeSchema } = require('./app/models/prizeModel');
const { voucherSchema } = require('./app/models/voucherModel');
const { voucherHistorySchema } = require('./app/models/voucherHistoryModel');
const { prizeHistorySchema } = require('./app/models/prizeHistoryModel');
const userRouter = require('./app/routes/userRouter');
const diceHistoryRouter = require('./app/routes/diceHistoryRouter');
const prizeRouter = require('./app/routes/prizeRouter');
const voucherRouter = require('./app/routes/voucherRouter');
const prizeHistoryRouter = require('./app/routes/prizeHistoryRouter');
const voucherHistoryRouter = require('./app/routes/voucherHistoryRouter');
const diceRouter = require('./app/routes/diceRouter');

const port = 8000;

app.use(luckyDiceMiddleware);

app.use(express.static(__dirname + "/views/Lucky Dice"));

app.get("/random-number", (req, res) => {
    res.json({
        message: Math.floor(Math.random() * 6)
    })
})
app.use(express.urlencoded({
    extended: true
}))
app.use(express.json());

mongoose.connect('mongodb://0.0.0.0:27017/CRUD_LuckyDice', () => {
    console.log('Connect to MongoDB Success');
})
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH');
    next();
});
app.get("/", (req, res) => {
    console.log(__dirname);
    // Hiển thị project ra localhost8000
    res.sendFile(path.join(__dirname + "/views/Lucky Dice/Task 23B.60.html"));
})
app.use(userRouter);
app.use(diceHistoryRouter);
app.use(prizeRouter);
app.use(voucherRouter);
app.use(prizeHistoryRouter);
app.use(voucherHistoryRouter);
app.use(diceRouter);

app.listen(port, () => {
    console.log("App listening on port: ", port);
});
app.use('/', (req, res) => {
    res.status(200).json({
        prizeHistories: prizeHistorySchema
    })
});
app.use('/', (req, res) => {
    res.status(200).json({
        voucherHistories: voucherHistorySchema
    })
});
app.use('/', (req, res) => {
    res.status(200).json({
        user: userSchema
    })
});
app.use('/', (req, res) => {
    res.status(200).json({
        diceHistory: diceHistorySchema
    })
});
app.use('/', (req, res) => {
    res.status(200).json({
        prize: prizeSchema
    })
});
app.use('/', (req, res) => {
    res.status(200).json({
        voucher: voucherSchema
    })
});
